# Copyright 2019: CAI Pengfei, GUO Mengxue. All Rights Reserved.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public version 2 or later
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ======================================================================
class SettingsParams:
    def __init__(self):
        self.centralWinTempsButton_font = "Arial"
        self.centralWinTitleButton_font = "Times New Roman"
        self.centralWin_subButton_font = "Times New Roman"
        self.centralWin_afficheDonne_font = "Times New Roman"
        self.moteurInfo_font = "Times New Roman"
        
        self.centralWinTempsButton_fontSize = "12px"
        self.centralWinTitleButton_fontSize = "12px"
        self.centralWin_subButton_fontSize = "12px"
        self.centralWin_afficheDonne_fontSize = "14px"
        self.moteurInfo_fontSize = "12px"

        # self.centralWinTempsButton_bgColor = "#48D1CC"
        self.centralWinTempsButton_bgColor = "#48D1CC"
        self.centralWinTitleButton_bgColor = "#48D1CC"
        self.centralWin_subButton_bgColor = "white"
        self.centralWin_afficheDonne_bgColor = "white"
        self.moteurInfo_bgColor = "white"

        self.centralWinTempsButton_fontColor = "white"
        self.centralWinTitleButton_fontColor = "white"
        self.centralWin_subButton_fontColor = "black"
        self.centralWin_afficheDonne_fontColor = "black"
        self.moteurInfo_fontColor = "black"

        self.affiche_color = {"instaneous":"MidnightBlue", "short_term_mean":"Blue",
                          "medium_term_mean":"DeepSkyBlue", "long_term_mean":"PowDerBlue"}


    def genererQssText(self):
        self.qss_centralWinTempsButton = "background-color: {}; color: {}; font-size: 12px;".format(self.centralWinTempsButton_bgColor, self.centralWinTempsButton_fontColor) \
                        + "font-weight: bold; font-family:{};".format(self.centralWinTempsButton_font) \
                        + "border-style: solid; padding: 2px; border-width: 2px;border-color:{};".format(self.centralWinTempsButton_bgColor)

        self.qss_centralWinTitleButton = "background-color: {}; color: white; font-size: 12px;".format(self.centralWinTitleButton_bgColor, self.centralWinTitleButton_fontColor) \
                        + "font-weight: bold; font-family:{};".format(self.centralWinTitleButton_font) \
                        + "border-style: solid; padding: 2px; border-width: 2px;border-color:{};".format(self.centralWinTitleButton_bgColor)

        self.qss_centralWin_subButton = "background-color: {}; color: {}; font-size: 12px;".format(self.centralWin_subButton_bgColor, self.centralWin_subButton_fontColor) \
                        + "font-family: {}; border-style: outset; padding: 2px;border-width: 1px; border-color: {};".format(self.centralWin_subButton_font, self.centralWinTitleButton_bgColor)
        
        self.qss_centralWin_afficheDonne = "background-color: {}; color: {}; font-size: 14px;".format(self.centralWin_afficheDonne_bgColor, self.centralWin_afficheDonne_fontColor) \
                        + "font-family: {}; border-style: outset; padding: 2px;border-width: 1px; border-color: {};".format(self.centralWin_afficheDonne_font, self.centralWinTitleButton_bgColor)
        
        self.qss_moteurInfo = "background-color: {}; color: {}; font-size: 14px;".format(self.moteurInfo_bgColor, self.moteurInfo_fontColor) \
                        + "font-family: {}; border-style: outset; padding: 2px;border-width: 1px; border-color: {};".format(self.moteurInfo_font, self.centralWinTitleButton_bgColor)


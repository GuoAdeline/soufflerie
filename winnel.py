# -*- coding: utf-8 -*-
import sys
import time
import numpy as np
import matplotlib.dates
import datetime


class Parameter(object):
    def __init__(self, value, name, unit, fmt="{0}", doc=""):
        try:
            self.value = float(value)
        except (TypeError, ValueError):
            self.value = value
        self.unit = unit
        self.name = name
        self._fmt = fmt
        self.doc = doc

    def __str__(self):
        return self.str_()
        # return str(self.value)

    def str(self):
        """Alternative formatting method, which uses an internal formatting string (like '.2f' ofr a float), and returns formatted value.
        That means a paticuliar type is expected as value.

        If value is an array or list, only first value is returned.
        """
        if self.value is None:
            return "{0}".format(self.value)
        elif isinstance(self.value, (list, np.ndarray)):
            return self._fmt.format(self.value[0])
        else:
            return self._fmt.format(self.value)

    def str_(self):
        """Alternative formatting method, which uses an internal formatting string (like '.2f' ofr a float), and returns formatted value.
        That means a paticuliar type is expected as value.

        If value is an array or list, only first value is returned.
        """
        if self.value is None:
            return "{0} {1}".format(self.value, self.unit)
        elif isinstance(self.value, (list, np.ndarray)):
            return (self._fmt + " {1}").format(self.value[0], self.unit)
        else:
            return (self._fmt + " {1}").format(self.value, self.unit)

    def str__(self):
        """Alternative formatting method, in the "<name> = <value>" form.

        If value is an array or list, value is not represented with its defaut str form, but as a space seperated list.
        """
        if self.value is None:
            return ""
        else:
            return "{0} = {1}".format(self.name, self.csv(" "))

    def csv(self, sep=";"):
        """Return the value for a csv representation. If value is an array or list, values are returned as a <sep> separated list of its values, otherwise it is same as using str() on value.
        """
        if self.value is None:
            return ""
        elif isinstance(self.value, (list, np.ndarray)):
            return sep.join([str(v) for v in self.value])
        else:
            return str(self.value)


class Timestamp(Parameter):
    def __init__(self, value, name, doc=""):
        if value is None:
            value = time.time()
        super().__init__(value, name, 's', doc=doc)

    def __str__(self):
        return self.toStr()

    def toMatplotlib(self):
        try:
            return matplotlib.dates.epoch2num(self.value)
        except TypeError:
            return matplotlib.dates.epoch2num(time.time())

    def toDatetime(self):
        try:
            datetime.datetime.utcfromtimestamp(self.value)
        except TypeError:
            datetime.datetime.utcnow()

    def toStr(self, fmt="%Y.%m.%d-%H.%M.%S"):
        try:
            return time.strftime(fmt, time.gmtime(self.value))
        except:
            return "None"


class Duration(Timestamp):
    def __init__(self, value, name, doc=""):
        super().__init__(value, name, doc)

    def toMatplotlib(self):
        if self.value is not None:
            return self.value
        else:
            return 0.

    def toDatetime(self):
        if self.value is not None:
            return datetime.timedelta(seconds=self.value)
        else:
            return datetime.timedelta()

    def toStr(self, fmt="{0:02d}:{1:02d}:{2:06.3f}"):
        h = int(self.value / 3600)
        m = int((self.value - h * 3600) / 60)
        s = self.value - h * 3600 - m * 60
        return fmt.format(h, m, s)


class Frequency(Parameter):
    def __init__(self, value, name):
        super().__init__(value, name, 'Hz', doc="sampling frequency")

    @property
    def period(self):
        if self.value:
            return 1 / self.value
        else:
            return 0.

    @period.setter
    def period(self, value):
        if value:
            self.value = 1 / value
        else:
            self.value = 0.

    def toPeriod(self, name=None):
        if name is None:
            return Duration(self.period, "period of " + self.name)
        else:
            return Duration(self.period, name)


class Params(object):
    def __init__(self, name):
        self.name = name
        self._timestamp = Timestamp(0., "BeginTime", "starting time of acquisition")
        self._duration = Duration(0., "Duration", "duration of acquisition")
        self._frequency = Frequency(0., "Fs")
        self._str_array_fmt = ".2f"
        self._csvList = ["timestamp", "duration", "frequency"]

    def _setter(self, value):
        try:
            return float(value)
        except (TypeError, ValueError):
            if value == "None":
                return None
            else:
                return value

    @property
    def timestamp(self):
        return self._timestamp.value

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp.value = self._setter(value)

    @property
    def duration(self):
        return self._duration.value

    @duration.setter
    def duration(self, value):
        self._duration.value = self._setter(value)

    @property
    def frequency(self):
        return self._frequency.value

    @frequency.setter
    def frequency(self, value):
        self._frequency.value = self._setter(value)

    def csv(self, sep=";", params=None):
        if params is None:
            params = self._csvList
        return sep.join([str(getattr(self, v, None)) for v in params])

    def head(self, full=None):
        if full is None:
            fmt = "{0} @ {1} (UTC)"
        elif full:
            fmt = "{0} @ {1} (UTC) / {2} @ {3}"
        else:
            fmt = "{0} @ {1} (UTC) / {2}"
        return fmt.format(self.name, self._timestamp, self._duration, self._frequency)

    def doc(self, sep="\n\t+ ", params=None):
        if params is None:
            params = self._csvList
        return self.name + " parameters :" + sep + sep.join(["{0} [{1}] : {2}".format(getattr(self, "_" + v, None).name,
                                                                                      getattr(self, "_" + v, None).unit,
                                                                                      getattr(self, "_" + v, None).doc)
                                                             for v in params])

    def _str_array(self, vname, v):
        return "{0} = {1}".format(vname, " ".join(list(v)))

    def _str_array_m(self, vname, v):
        return "{0} = {1}".format(vname, v[0])


class WindBoxParams(Params):
    def __init__(self):
        super().__init__("WindBox")
        self._dPflow = Parameter(None, "dPflow", "Pa", "{0:.1f}", "differential pressure from Prandtl probe")
        self._Tflow = Parameter(None, "Tflow", "°C", "{0:.1f}", "temperature measured in flow")
        self._Pflow = Parameter(None, "Pflow", "Pa", "{0:.0f}",
                                "absolute pressure measured in flow on Prandtl probe's static path")
        self._Uflow = Parameter(None, "Uflow", "m/s", "{0:.1f}", "flow's speed computed from dPflow, Pflow, Tflow")
        self._U_flow = Parameter(None, "U<>flow", "m/s", "{0:.1f}",
                                 "flow's speed computed from mean values of dPflow, Pflow, Tflow")
        self._Patm = Parameter(None, "Patm", "Pa", "{0:.0f}", "atmospheric pressure measured in hall")
        self._Tatm = Parameter(None, "Tatm", "°C", "{0:.1f}", "atmospheric temperature measured in hall")
        self._RHatm = Parameter(None, "RHatm", "%", "{0:.0f}", "atmospheric relative humidity measured in hall")
        self._csvList += ["Uflow", "U_flow", "dPflow", "Pflow", "Tflow", "Patm", "Tatm", "RHatm"]

    @property
    def dPflow(self):
        return self._dPflow.value

    @dPflow.setter
    def dPflow(self, value):
        self._dPflow.value = self._setter(value)

    @property
    def Tflow(self):
        return self._Tflow.value

    @Tflow.setter
    def Tflow(self, value):
        self._Tflow.value = self._setter(value)

    @property
    def Pflow(self):
        return self._Pflow.value

    @Pflow.setter
    def Pflow(self, value):
        self._Pflow.value = self._setter(value)

    @property
    def Uflow(self):
        return self._Uflow.value

    @Uflow.setter
    def Uflow(self, value):
        self._Uflow.value = self._setter(value)

    @property
    def U_flow(self):
        return self._U_flow.value

    @U_flow.setter
    def U_flow(self, value):
        self._U_flow.value = self._setter(value)

    @property
    def Patm(self):
        return self._Patm.value

    @Patm.setter
    def Patm(self, value):
        self._Patm.value = self._setter(value)

    @property
    def Tatm(self):
        return self._Tatm.value

    @Tatm.setter
    def Tatm(self, value):
        self._Tatm.value = self._setter(value)

    @property
    def RHatm(self):
        return self._RHatm.value

    @RHatm.setter
    def RHatm(self, value):
        self._RHatm.value = self._setter(value)

    def __str__(self):
        return "Windbox @ {0} / {1}\n  {2} {3} {4} {5} {6}\n  {7} {8} {9}".format(self._timestamp, self._duration,
                                                                                  self._Uflow, self._U_flow,
                                                                                  self._dPflow, self._Pflow,
                                                                                  self._Tflow,
                                                                                  self._Patm, self._Tatm, self._RHatm)

    def str(self, sep=" | ", title=False):
        params = ["_Uflow", "_U_flow", "_dPflow", "_Pflow", "_Tflow", "_Patm", "_Tatm", "_RHatm"]
        if title:
            return sep.join(["{0:^10s}".format(v[1:]) for v in params])
        else:
            return sep.join(["{0:^10s}".format(getattr(self, v, None).str()) for v in params])

    def check(self):
        return hasattr(self, "dPf")


class WindTunnelParams(Params):
    def __init__(self):
        super().__init__("Fan")
        self._RPM = Parameter(None, "RPM", "RPM", "{0:.0f}", "motor's speed, requested value (order)")
        self._rpm = Parameter(None, "RPM_m", "RPM", "{0:.1f}", "motor's speed, measured value")
        self._status = Parameter(None, "status", "", doc="flag, whether all is OK or not")
        self._controlled = Parameter(None, "control", "",
                                     doc="flag, whether motor is under automatic control or not (i.e. manual control)")
        self._message = Parameter(None, "message", "", doc="status message")
        self._powered = Parameter(None, "power", "", doc="flag, whether motor (variator) is powered or not")
        self._csvList += ["RPM", "rpm", "status"]

    def __str__(self):
        return "{0} / {1} [{2}]".format(self._RPM, self._rpm, self.status)

    @property
    def RPM(self):
        return self._RPM.value

    @RPM.setter
    def RPM(self, value):
        self._RPM.value = self._setter(value)

    @property
    def rpm(self):
        return self._rpm.value

    @rpm.setter
    def rpm(self, value):
        self._rpm.value = self._setter(value)

    @property
    def status(self):
        return self._status.value

    @status.setter
    def status(self, value):
        self._status.value = self._setter(value)

    @property
    def controlled(self):
        return self._controlled.value

    @controlled.setter
    def controlled(self, value):
        self._controlled.value = self._setter(value)

    @property
    def message(self):
        return self._message.value

    @message.setter
    def message(self, value):
        self._message.value = self._setter(value)

    @property
    def powered(self):
        return self._powered.value

    @powered.setter
    def powered(self, value):
        self._powered.value = self._setter(value)


class DataFile(object):
    def __init__(self):
        self.filepath = None
        self.filedesc = None

        self.cfgParams = None
        self.envParams = None

    def _setCfgFromFileName(self):
        pass


class WindTunnel(DataFile):
    def _setCfgFromFileName(self, filename):
        p = WindTunnelParams()


if __name__ == "__main__":
    print("test")
    # w = WindBoxParams()
    my = {"instaneous":WindBoxParams(),  "short_term_mean": WindBoxParams(),
                             "medium_term_mean": WindBoxParams(), "long_term_mean": WindBoxParams()}
    print(my["instaneous"])
    my["instaneous"].dPflow = 1.3
    print(my["instaneous"])


    # print(w)
    # w.dPflow = 1.3  # <=> w._dPflow.value = 1.3
    # w.timestamp = None
    # w.Tflow = 23.567
    # w.Tatm = "10.987654321"
    # print(w)
    #
    # print(w.dPflow * 2 +0.0001)
    # print(w._dPflow)
    # sys.stdout.write(str(w._dPflow))
    # print(w._dPflow.str_())
    # print(w._dPflow.str__())
    # print(w._dPflow.csv())
    #
    # print(w.csv())
    # print(w.csv(" "))
    # print(w.head())
    # print(w.str(title=True))
    # print(w.str())
    # print(w.doc())



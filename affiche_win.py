# Copyright 2019: CAI Pengfei, GUO Mengxue. All Rights Reserved.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public version 2 or later
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ======================================================================

import PyQt5.QtWidgets as PyQW
import PyQt5.QtGui as PyQGui
import PyQt5.QtCore as PyQCore
import matplotlib
import matplotlib.dates as mdates
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')

import numpy as np
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import data_skeleton
from subwins import holdScale

class MotherMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = self.fig.add_subplot(111)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self, PyQW.QSizePolicy.Expanding, PyQW.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        # self.plot()
    def settingsForFig(self):
        minuteLoc = mdates.MinuteLocator()
        secondLoc = mdates.SecondLocator()
        majorFormatter_1 = mdates.DateFormatter("\n%H:%M:%S")
        minorFormatter_1 = mdates.DateFormatter("%S")


        self.axes.xaxis.set_major_locator(minuteLoc)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.xaxis.set_major_formatter(majorFormatter_1)
        self.axes.xaxis.set_minor_formatter(minorFormatter_1)
        self.axes.set_xlabel("time (s)")
        self.axes.grid(True)

    def compute_initial_figure(self):
        pass

class PressionMplCanvas(MotherMplCanvas):
    def __init__(self, timer, dataH, preferences, parent=None, width=5, height=4, dpi=100):
        self.timer=timer
        self.dataH = dataH
        self.preferences = preferences
        MotherMplCanvas.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        self.timer.timeout.connect(self.update_figure)

    def compute_initial_figure(self):
        self.settingsForFig()
        self.axes.set_ylim(80000, 130000)
        self.axes.set_ylabel("Pression (Pa)")
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # self.axes.plot([mdates.epoch2num(time.time())],[100000.0])
        # self.axes.plot([0.0], [100000.0], 'r')


    def update_figure(self):
        self.axes.cla()
        self.settingsForFig()
        secondLoc = mdates.SecondLocator(interval=1)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.set_ylim(80000,130000 )
        self.axes.set_ylabel("Pression (Pa)")
        n_show = int(self.preferences.historique_affiche/self.preferences.timeScan)

        for key in self.preferences.terme_affiche:
            self.axes.plot(self.dataH.wind_data[key]._timestamp.toMatplotlib()[-n_show:],
                           self.dataH.wind_data[key].Pflow[-n_show:],self.preferences.otherSettings.affiche_color[key],  label=key)
        # Put a legend to the right of the current axis
        self.axes.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        self.draw()

class TemperatureMplCanvas(MotherMplCanvas):
    def __init__(self, timer, dataH, preferences, parent=None, width=5, height=4, dpi=100):
        self.timer=timer
        self.dataH = dataH
        self.preferences = preferences
        MotherMplCanvas.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        self.timer.timeout.connect(self.update_figure)

    def compute_initial_figure(self):
        self.settingsForFig()
        self.axes.set_ylim(-20, 60)
        self.axes.set_ylabel("Temperature (Pa)")
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # self.axes.plot([mdates.epoch2num(time.time())],[100000.0])
        # self.axes.plot([0.0], [100000.0], 'r')


    def update_figure(self):
        self.axes.cla()
        self.settingsForFig()
        secondLoc = mdates.SecondLocator(interval=1)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.set_ylim(-20, 60 )
        self.axes.set_ylabel("Temperature (Pa)")
        n_show = int(self.preferences.historique_affiche/self.preferences.timeScan)
        color = {"instaneous": "MidnightBlue", "short_term_mean": "Blue",
                 "medium_term_mean": "DeepSkyBlue", "long_term_mean": "PowDerBlue"}

        for key in self.preferences.terme_affiche:
            self.axes.plot(self.dataH.wind_data[key]._timestamp.toMatplotlib()[-n_show:],
                           self.dataH.wind_data[key].Tflow[-n_show:],self.preferences.otherSettings.affiche_color[key],  label=key)
        # Put a legend to the right of the current axis
        self.axes.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        self.draw()


class PressionDiffMplCanvas(MotherMplCanvas):
    def __init__(self, timer, dataH, preferences, parent=None, width=5, height=4, dpi=100):
        self.timer=timer
        self.dataH = dataH
        self.preferences = preferences
        MotherMplCanvas.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        self.timer.timeout.connect(self.update_figure)

    def compute_initial_figure(self):
        self.settingsForFig()
        self.axes.set_ylim(-20, 20)
        self.axes.set_ylabel("Pression Diff (Pa)")
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # self.axes.plot([mdates.epoch2num(time.time())],[100000.0])
        # self.axes.plot([0.0], [100000.0], 'r')


    def update_figure(self):
        self.axes.cla()
        self.settingsForFig()
        secondLoc = mdates.SecondLocator(interval=1)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.set_ylim(-20, 20 )
        self.axes.set_ylabel("Pression Diff (Pa)")
        n_show = int(self.preferences.historique_affiche/self.preferences.timeScan)

        for key in self.preferences.terme_affiche:
            self.axes.plot(self.dataH.wind_data[key]._timestamp.toMatplotlib()[-n_show:],
                           self.dataH.wind_data[key].dPflow[-n_show:],self.preferences.otherSettings.affiche_color[key],  label=key)
        # Put a legend to the right of the current axis
        self.axes.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        self.draw()
        

class HumiditeRelMplCanvas(MotherMplCanvas):
    def __init__(self, timer, dataH, preferences, parent=None, width=5, height=4, dpi=100):
        self.timer=timer
        self.dataH = dataH
        self.preferences = preferences
        MotherMplCanvas.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        self.timer.timeout.connect(self.update_figure)

    def compute_initial_figure(self):
        self.settingsForFig()
        self.axes.set_ylim(0, 100)
        self.axes.set_ylabel("Humidite Relative (%)")
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # self.axes.plot([mdates.epoch2num(time.time())],[100000.0])
        # self.axes.plot([0.0], [100000.0], 'r')


    def update_figure(self):
        self.axes.cla()
        self.settingsForFig()
        secondLoc = mdates.SecondLocator(interval=1)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.set_ylim(0, 100 )
        self.axes.set_ylabel("Humidite Relative (%)")
        n_show = int(self.preferences.historique_affiche/self.preferences.timeScan)

        for key in self.preferences.terme_affiche:
            self.axes.plot(self.dataH.wind_data[key]._timestamp.toMatplotlib()[-n_show:],
                           list(map(lambda x: x * 100, self.dataH.wind_data[key].RHatm[-n_show:])), self.preferences.otherSettings.affiche_color[key],  label=key)



        # Put a legend to the right of the current axis
        self.axes.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        self.draw()

class VitesseEcoulementMplCanvas(MotherMplCanvas):
    def __init__(self, timer, dataH, preferences, parent=None, width=5, height=4, dpi=100):
        self.timer=timer
        self.dataH = dataH
        self.preferences = preferences
        MotherMplCanvas.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        self.timer.timeout.connect(self.update_figure)

    def compute_initial_figure(self):
        self.settingsForFig()
        self.axes.set_ylim(0, 400)
        self.axes.set_ylabel("Vitesse d'écoulement de référence (m/s)")
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # self.axes.plot([mdates.epoch2num(time.time())],[100000.0])
        # self.axes.plot([0.0], [100000.0], 'r')


    def update_figure(self):
        self.axes.cla()
        self.settingsForFig()
        secondLoc = mdates.SecondLocator(interval=1)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.set_ylim(0, 400 )
        self.axes.set_ylabel("Vitesse d'écoulement de référence (m/s)")
        n_show = int(self.preferences.historique_affiche/self.preferences.timeScan)

        for key in self.preferences.terme_affiche:
            self.axes.plot(self.dataH.wind_data[key]._timestamp.toMatplotlib()[-n_show:],
                           self.dataH.wind_data[key].Uflow[-n_show:], self.preferences.otherSettings.affiche_color[key], label=key)



        # Put a legend to the right of the current axis
        self.axes.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        self.draw()

class VitesseEcoulementMeanMplCanvas(MotherMplCanvas):
    def __init__(self, timer, dataH, preferences, parent=None, width=5, height=4, dpi=100):
        self.timer=timer
        self.dataH = dataH
        self.preferences = preferences
        MotherMplCanvas.__init__(self, parent=parent, width=width, height=height, dpi=dpi)
        self.timer.timeout.connect(self.update_figure)

    def compute_initial_figure(self):
        self.settingsForFig()
        self.axes.set_ylim(0, 400)
        self.axes.set_ylabel("Mean de vitesse d'écoulement de référence(m/s)")
        box = self.axes.get_position()
        self.axes.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # self.axes.plot([mdates.epoch2num(time.time())],[100000.0])
        # self.axes.plot([0.0], [100000.0], 'r')


    def update_figure(self):
        self.axes.cla()
        self.settingsForFig()
        secondLoc = mdates.SecondLocator(interval=1)
        self.axes.xaxis.set_minor_locator(secondLoc)
        self.axes.set_ylim(0, 400 )
        self.axes.set_ylabel("Mean de vitesse d'écoulement de référence (m/s)")
        n_show = int(self.preferences.historique_affiche/self.preferences.timeScan)

        for key in self.preferences.terme_affiche:
            self.axes.plot(self.dataH.wind_data[key]._timestamp.toMatplotlib()[-n_show:],
                           self.dataH.wind_data[key].U_flow[-n_show:], self.preferences.otherSettings.affiche_color[key], label=key)



        # Put a legend to the right of the current axis
        self.axes.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        self.draw()



class PressionWin(PyQW.QWidget):
    def __init__(self, timer, dataH, preferences):
        super().__init__()
        self.timer = timer
        self.dataH = dataH
        self.preferences = preferences
        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 900, holdScale(900))
        self.setWindowTitle("Pression")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.layout = PyQW.QVBoxLayout(self)
        dc = PressionMplCanvas(self.timer, self.dataH, self.preferences, self, width=5, height=4, dpi=100)
        self.layout.addWidget(dc)

class TemperatureWin(PyQW.QWidget):
    def __init__(self, timer, dataH, preferences):
        super().__init__()
        self.timer = timer
        self.dataH = dataH
        self.preferences = preferences
        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 900, holdScale(900))
        self.setWindowTitle("Température")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.layout = PyQW.QVBoxLayout(self)
        dc = TemperatureMplCanvas(self.timer, self.dataH, self.preferences, self, width=5, height=4, dpi=100)
        self.layout.addWidget(dc)
        
        
class PressionDiffWin(PyQW.QWidget):
    def __init__(self, timer, dataH, preferences):
        super().__init__()
        self.timer = timer
        self.dataH = dataH
        self.preferences = preferences
        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 900, holdScale(900))
        self.setWindowTitle("Pression Diff")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.layout = PyQW.QVBoxLayout(self)
        dc = PressionDiffMplCanvas(self.timer, self.dataH, self.preferences, self, width=5, height=4, dpi=100)
        self.layout.addWidget(dc)
        
        
class HumiditeRelWin(PyQW.QWidget):
    def __init__(self, timer, dataH, preferences):
        super().__init__()
        self.timer = timer
        self.dataH = dataH
        self.preferences = preferences
        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 900, holdScale(900))
        self.setWindowTitle("Humidité Relative")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.layout = PyQW.QVBoxLayout(self)
        dc = HumiditeRelMplCanvas(self.timer, self.dataH, self.preferences, self, width=5, height=4, dpi=100)
        self.layout.addWidget(dc)


class VitesseEcoulementWin(PyQW.QWidget):
    def __init__(self, timer, dataH, preferences):
        super().__init__()
        self.timer = timer
        self.dataH = dataH
        self.preferences = preferences
        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 900, holdScale(900))
        self.setWindowTitle("Vitesse d'écoulement de référence")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.layout = PyQW.QVBoxLayout(self)
        dc = VitesseEcoulementMplCanvas(self.timer, self.dataH, self.preferences, self, width=5, height=4, dpi=100)
        self.layout.addWidget(dc)
        
class VitesseEcoulementMeanWin(PyQW.QWidget):
    def __init__(self, timer, dataH, preferences):
        super().__init__()
        self.timer = timer
        self.dataH = dataH
        self.preferences = preferences
        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 900, holdScale(900))
        self.setWindowTitle("Mean de vitesse d'écoulement de référence")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.layout = PyQW.QVBoxLayout(self)
        dc = VitesseEcoulementMeanMplCanvas(self.timer, self.dataH, self.preferences, self, width=5, height=4, dpi=100)
        self.layout.addWidget(dc)
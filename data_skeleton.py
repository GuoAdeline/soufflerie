# -*- coding: utf-8 -*-


import winnel
import time
import random


def rand_abs_pressure() :
    return (random.random() * 30000.) + 90000.

# skeleton class
class LiveData(object) :
    def __init__(self) : # add here necessary parameters
        self.type = 0 # ???
        # self.wind_data = winnel.WindBoxParams()
        # [instantaneous, short_term_mean, medium_term_mean, long_term_mean]
        # [ 1s, 10s, 60s, [600s] ]
        self.wind_data = {"instaneous":winnel.WindBoxParams(), "short_term_mean":winnel.WindBoxParams(),
                          "medium_term_mean":winnel.WindBoxParams(), "long_term_mean":winnel.WindBoxParams()}
        self.tunnel_data = {"instaneous":winnel.WindTunnelParams(), "short_term_mean":winnel.WindTunnelParams(),
                          "medium_term_mean":winnel.WindTunnelParams(), "long_term_mean":winnel.WindTunnelParams()}
        #self.wind_data = [ winnel.WindBoxParams(), winnel.WindBoxParams(), winnel.WindBoxParams() ]
        #self.wind_data_1 = winnel.WindBoxParams()
        #self.wind_data_10 = winnel.WindBoxParams()
        #self.wind_data_60 = winnel.WindBoxParams()
        # self.tunnel_data = winnel.WindTunnelParams()

    def update(self) :
        self.wind_data["instaneous"].timestamp = time.time() # current time
        self.tunnel_data["instaneous"].timestamp = time.time()


# random class, containg fake but reasonable data
class LiveData_random(LiveData) :
    def update(self) :
        # wind_data : parameters are :
        # dPflow, Pflow, Tflow, Tatm, RHatm, Patm, Uflow, U_flow

        for key in self.wind_data.keys():
            self.wind_data[key].timestamp = time.time() # current time
            self.wind_data[key].Pflow = rand_abs_pressure()
            self.wind_data[key].Patm = rand_abs_pressure()
            self.wind_data[key].Tflow = 25 + rand_abs_pressure()/100000*3
            self.wind_data[key].Tatm = 25 + rand_abs_pressure()/100000*3
            self.wind_data[key].RHatm = random.uniform(0,1)
            self.wind_data[key].dPflow = random.uniform(-20, 20)
            self.wind_data[key].U_flow = random.randint(10,400)
            self.wind_data[key].Uflow = random.randint(10,400)
    
    
            # and so on
            # tunnel_data :
            # RPM (order), rpm (measure)
            self.tunnel_data[key].timestamp = time.time()
            self.tunnel_data[key].RPM = 100.
            self.tunnel_data[key].rpm = 100. + random.random()*10-5

            # powered: On: green, Off: red
            if random.randint(1, 2) == 1:
                self.tunnel_data["instaneous"].powered = "On"   #green
            else:
                self.tunnel_data["instaneous"].powered = "Off"      #red
            # controlled Auto: green, Non-auto:red
            if random.randint(1, 2) == 1:
                self.tunnel_data["instaneous"].controlled = "Auto"      #green
            else:
                self.tunnel_data["instaneous"].controlled = "Non-auto"      #red
            # status: True: green, False:grey, None:orange
            if random.randint(1, 3) == 1:
                self.tunnel_data["instaneous"].status = True     #green
            elif random.randint(1, 3) == 2:
                self.tunnel_data["instaneous"].status = False     #grey
            else:
                self.tunnel_data["instaneous"].status = None    #orange

            # self.tunnel_data[key].powered = "on"
            # self.tunnel_data[key].controlled = "Auto"
            # self.tunnel_data[key].status = "Ok"
            self.tunnel_data[key].message = "Everthing is ok."


# random class, containg fake but reasonable data
class HistData(LiveData) :
    def __init__(self) : # add here necessary parameters
        super().__init__()
        # memory length
        self.n_hist = 70 # 5 for test => more : 100 ?

    def set_n_hist(self, n_hist):
        self.n_hist = n_hist

    def history(self) :
        """set initial history"""
        # wind_data["instaneous"] : parameters are :
        # dPflow, Pflow, Tflow, Tatm, RHatm, Patm, Uflow, U_flow
        # data are lists
        for key in self.wind_data.keys():
            self.wind_data[key].timestamp = []
            self.wind_data[key].Pflow = []
            self.wind_data[key].Patm = []
            self.wind_data[key].Tflow = []
            self.wind_data[key].Tatm = []
            self.wind_data[key].RHatm = []
            self.wind_data[key].dPflow = []
            self.wind_data[key].U_flow = []
            self.wind_data[key].Uflow = []

    def update(self, wdata) :
        # wind_data["instaneous"] : parameters are :
        # dPflow, Pflow, Tflow, Tatm, RHatm, Patm, Uflow, U_flow
        # wdata: type---LiveData.wind_data
        for key in self.wind_data.keys():
            self.wind_data[key]._timestamp.value.append( wdata[key].timestamp )
            self.wind_data[key]._Pflow.value.append( wdata[key].Pflow )
            self.wind_data[key]._Tflow.value.append(wdata[key].Tflow)
            self.wind_data[key]._dPflow.value.append(wdata[key].dPflow)
            self.wind_data[key]._RHatm.value.append(wdata[key].RHatm)
            self.wind_data[key]._U_flow.value.append(wdata[key].U_flow)
            self.wind_data[key]._Uflow.value.append(wdata[key].Uflow)
            if len(self.wind_data[key]._timestamp.value) > self.n_hist :
                self.wind_data[key]._timestamp.value.pop(0)
                self.wind_data[key]._Pflow.value.pop(0)
                self.wind_data[key]._Tflow.value.pop(0)
                self.wind_data[key]._dPflow.value.pop(0)
                self.wind_data[key]._RHatm.value.pop(0)
                self.wind_data[key]._U_flow.value.pop(0)
                self.wind_data[key]._Uflow.value.pop(0)



class HistData_random(HistData) :
    def history(self) :
        """set initial history"""
        super().history()
        start_time= time.time()
        for i in range(4) :
            self.wind_data["instaneous"]._timestamp.value.append( start_time - self.n_hist + i )
            self.wind_data["instaneous"]._Pflow.value.append( rand_abs_pressure() ) # or whatever ra&ndom number



if __name__ == "__main__" :
    # data = LiveData_random()
    # data.update()
    # print(data.wind_data["instaneous"])
    # print(data.wind_data)
    # print(data.wind_data._timestamp.toStr())
    # print(data.wind_data._timestamp.toStr(fmt="%Y-%m-%d\n%H:%M:%S"))
    # print(data.wind_data._timestamp.toMatplotlib(), data.wind_data.Pflow)
    #
    # dataH = HistData_random()
    # dataH.history() # initial setting
    # print(dataH.wind_data["instaneous"]._timestamp.toMatplotlib(), dataH.wind_data["instaneous"].Pflow)
    # print(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow)
    # # while history plot :
    # for i in range(5) :
    #     data.update()
    #     dataH.update(data.wind_data)
    #     print(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow)
    #     # matplotlib.plot_date(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow) # 1s
    #     # matplotlib.plot_date(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow) # 10s
    #     # matplotlib.plot_date(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow) # 60s

    dataH = HistData()
    dataH.history()
    for i in range(1000) :
        data = LiveData_random()
        data.update()
        print(data.wind_data["instaneous"].timestamp)
        dataH.update(data.wind_data)
        print(dataH.wind_data["instaneous"]._timestamp.toMatplotlib(), dataH.wind_data["instaneous"].Pflow)
        # matplotlib.plot_date(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow) # 1s
        # matplotlib.plot_date(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow) # 10s
        # matplotlib.plot_date(dataH.wind_data._timestamp.toMatplotlib(), dataH.wind_data.Pflow) # 60s


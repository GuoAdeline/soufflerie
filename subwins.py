# Copyright 2019: CAI Pengfei, GUO Mengxue. All Rights Reserved.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public version 2 or later
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ======================================================================
import PyQt5.QtWidgets as PyQW
import PyQt5.QtGui as PyQGui
import PyQt5.QtCore as PyQCore
import matplotlib

matplotlib.use('Qt5Agg')
import data_skeleton
import settings


def holdScale(width):
    return width / 1366 * 768

class AboutWin(PyQW.QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):

        self.ajouterBackground()
        self.ajouterOk()
        self.ajouterContenu()

        self.setGeometry(700, 300, 400, holdScale(400))
        self.setWindowTitle('A Propos')
        self.setWindowIcon(PyQGui.QIcon('./figs/question.png'))
        # self.show();

    def ajouterBackground(self):
        mypalette = PyQGui.QPalette()
        mypalette.setBrush(self.backgroundRole(),
                           PyQGui.QBrush(PyQGui.QPixmap('./figs/sky.jpg').scaledToWidth(400)))
        self.setPalette(mypalette)
        self.setAutoFillBackground(True)

    def ajouterContenu(self):
        self.label = PyQW.QLabel(self)
        self.label.setFixedWidth(300)
        self.label.setFixedHeight(80)
        self.label.setAlignment(PyQCore.Qt.AlignCenter)
        self.label.setText(u"Auteurs: CAI & GUO\n\n" u"Version: 1.0")
        pe = PyQGui.QPalette()
        pe.setColor(PyQGui.QPalette.WindowText, PyQCore.Qt.white)
        self.label.setPalette(pe)

        self.label.setFont(PyQGui.QFont("Arial", 10, PyQGui.QFont.Bold))
        self.label.move(55, 60)

    def ajouterOk(self):
        okButton = PyQW.QPushButton("OK")
        #okButton.clicked.connect(qApp.exit)
        quit = okButton
        quit.clicked.connect(self.close)

        hbox = PyQW.QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(okButton)

        vbox = PyQW.QVBoxLayout()
        vbox.addStretch(1)
        vbox.addLayout(hbox)

        self.setLayout(vbox)

        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('Buttons')
    #    self.show()

class PreferenceWin(PyQW.QWidget):
    def __init__(self, timer2):
        super(PreferenceWin,self).__init__()
        self.timer2 = timer2
        # default value
        self.timeScan = 5;
        self.ipAddr = "192.168.1.1"
        self.port = 5239
        self.historique_affiche = 25
        self.terme_affiche = ["instaneous", "short_term_mean", "medium_term_mean", "long_term_mean"]
        self.otherSettings = settings.SettingsParams()

        self.initUI()

    def initUI(self):
        self.setGeometry(700, 300, 400, holdScale(400))
        self.setWindowTitle("Préférences")
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))


        self.createGridGroupBox()
        mainLayout = PyQW.QVBoxLayout()

        hboxLayout = PyQW.QHBoxLayout()
        hboxLayout.addWidget(self.gridGroupBox)

        mainLayout.addLayout(hboxLayout)
        self.setLayout(mainLayout)

    def createGridGroupBox(self):
        self.gridGroupBox = PyQW.QGroupBox()
        layout = PyQW.QGridLayout()

        lb1 = PyQW.QLabel("Période de scan", self)
        self.sp1 = PyQW.QSpinBox(self)
        lb2 = PyQW.QLabel("Adresse IP(v4)", self)

        lb3 = PyQW.QLabel("Port", self)
        self.sp3 = PyQW.QSpinBox(self)
        lb4 = PyQW.QLabel("Durée d'historique affiché", self)
        self.sp4 = PyQW.QSpinBox(self)

        self.sp1.setRange(1, 200)
        self.sp1.setSingleStep(1)
        self.sp1.setValue(5)
        self.sp1.setSuffix(" secondes")

        self.textbox = PyQW.QLineEdit(self)
        self.sp3.setRange(0, 70000)
        self.sp3.setValue(10)
        self.sp3.setValue(5239)

        self.sp4.setRange(1, 10000)
        self.sp4.setValue(25)
        self.sp4.setSuffix(" secondes")

        lb5 = PyQW.QLabel("Que voulez-vous affiché dans diagramme:")
        self.cb_inst = PyQW.QCheckBox("instaneous")
        self.cb_short = PyQW.QCheckBox("short_term_mean")
        self.cb_medium = PyQW.QCheckBox("medium_term_mean")
        self.cb_long = PyQW.QCheckBox("long_term_mean")

        self.cb_inst.setChecked(True)
        self.cb_short.setChecked(True)
        self.cb_medium.setChecked(True)
        self.cb_long.setChecked(True)

        self.cb_inst.stateChanged.connect(self.changecb_inst)
        self.cb_short.stateChanged.connect(self.changecb_short)
        self.cb_medium.stateChanged.connect(self.changecb_medium)
        self.cb_long.stateChanged.connect(self.changecb_long)
        # [instaneous, short_term_mean, medium_term_mean, long_term_mean]

        self.buttonBox = PyQW.QDialogButtonBox(self)
        self.buttonBox.setOrientation(PyQCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(PyQW.QDialogButtonBox.Cancel | PyQW.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.ignore)


        self.setGeometry(300, 300, 300, 150)

        layout.addWidget(lb1, 1, 0)
        layout.addWidget(self.sp1, 1, 1)
        layout.addWidget(lb2, 2, 0)
        layout.addWidget(self.textbox, 2, 1)

        layout.addWidget(lb3, 3, 0)
        layout.addWidget(self.sp3, 3, 1)
        layout.addWidget(lb4, 4, 0)
        layout.addWidget(self.sp4, 4, 1)
        layout.addWidget(lb5, 5, 0)
        layout.addWidget(self.cb_inst, 5, 1)
        layout.addWidget(self.cb_short, 6, 1)
        layout.addWidget(self.cb_medium, 7,1)
        layout.addWidget(self.cb_long, 8, 1)


        layout.addWidget(self.buttonBox, 9, 1)

        self.gridGroupBox.setLayout(layout)

    # def onActivated(self, text):
    #     self.tmpIntervalle = self.intervalleChoice[text]
    #     self.lb4.setText("Intervalle d'échantillonnage {}s".format(self.tmpIntervalle))

    def accept(self):
        self.timeScan = self.sp1.value()
        if self.textbox.text()!="":
            self.ipAddr = self.textbox.text()
        self.port = self.sp3.value()
        # self.intervalle = self.tmpIntervalle
        self.historique_affiche = self.sp4.value()
        self.timer2.start(self.timeScan*1000)
        self.close()
    def ignore(self):

        self.close()
    def warningMsg(self, msg):
        reply = PyQW.QMessageBox.information(self,  # 使用infomation信息框
                                        "Warning",
                                        msg,
                                        PyQW.QMessageBox.Ok)


    def changecb_inst(self):
        existStatus = False
        for name in self.terme_affiche:
            if name == "instaneous":
                existStatus=True
        if self.cb_inst.isChecked() and existStatus == False:
            self.terme_affiche.append("instaneous")
        if not self.cb_inst.isChecked() and existStatus == True:
            self.terme_affiche.remove("instaneous")
    def changecb_short(self):
        existStatus = False
        for name in self.terme_affiche:
            if name == "short_term_mean":
                existStatus=True
        if self.cb_short.isChecked() and existStatus == False:
            self.terme_affiche.append("short_term_mean")
        if not self.cb_short.isChecked() and existStatus == True:
            self.terme_affiche.remove("short_term_mean")
    def changecb_medium(self):
        existStatus = False
        for name in self.terme_affiche:
            if name == "medium_term_mean":
                existStatus=True
        if self.cb_medium.isChecked() and existStatus == False:
            self.terme_affiche.append("medium_term_mean")
        if not self.cb_medium.isChecked() and existStatus == True:
            self.terme_affiche.remove("medium_term_mean")
    def changecb_long(self):
        existStatus = False
        for name in self.terme_affiche:
            if name == "long_term_mean":
                existStatus=True
        if self.cb_long.isChecked() and existStatus == False:
            self.terme_affiche.append("long_term_mean")
        if not self.cb_long.isChecked() and existStatus == True:
            self.terme_affiche.remove("long_term_mean")



# central widget in main window
class centralWin(PyQW.QWidget):
    def __init__(self, timer1, timer2, dataH, signalEnsemble, preferences):
        super().__init__()
        # self.dataH = data_skeleton.HistData()
        # self.dataH.history()
        self.dataH = dataH
        self.signalEnsemble = signalEnsemble
        self.preferences = preferences
        self.preferences.otherSettings.genererQssText()

        self.layout = PyQW.QVBoxLayout(self)
        layoutH = PyQW.QHBoxLayout(self)
        self.timeReelButton = PyQW.QPushButton("Temps Actuel\n---\n---", self)
        self.timeLastShowButton = PyQW.QPushButton("Temps Mesure\n---\n---",self)

        self.timeReelButton.setStyleSheet("QPushButton {"+self.preferences.otherSettings.qss_centralWinTempsButton+"}")
        self.timeLastShowButton.setStyleSheet("QPushButton {"+self.preferences.otherSettings.qss_centralWinTempsButton+"}")
        layoutH.addWidget(self.timeReelButton)
        layoutH.addWidget(self.timeLastShowButton)
        self.layout.addLayout(layoutH)


        # self.timer1 = PyQCore.QTimer(self)
        # self.timer1.start(1000)    # every 1000 millisecond(1s), update button text
        self.timer1 = timer1
        self.timer1.timeout.connect(self.showTimeReel)

        # self.timer2 = PyQCore.QTimer(self)
        # self.timer2.start(1000)
        self.timer2 = timer2
        self.timer2.timeout.connect(self.showTimeLastUpdate)

        self.layout.addWidget(self.addVitesseMoteurWidget())
        # self.layout.addWidget(self.addMoteurInfoWidget())
        self.layout.addWidget(self.addTemperatureAirWidget())
        self.layout.addWidget(self.addHumiditeRelativeWidget())
        self.layout.addWidget(self.addPressionAtmoWidget())
        # self.layout.addWidget(self.adddPFlowWidget())
        self.layout.addWidget(self.addVitesseEcoulementWidget())

        self.timer2.timeout.connect(lambda: self.updateAllParam())

        self.setLayout(self.layout)
    def updateAllParam(self):
        data = data_skeleton.LiveData_random()
        data.update()
        self.dataH.update(data.wind_data)
        # print(self.dataH.wind_data["instaneous"]._timestamp.toMatplotlib(), self.dataH.wind_data["instaneous"].Pflow)
        # wind data
        self.pressAtomLabel_souff.setText("{0:.0f} Pa".format(data.wind_data["instaneous"].Pflow));
        self.pressAtomLabel_hall.setText("{0:.0f} Pa".format(data.wind_data["instaneous"].Patm));
        self.temperatureAirLabel_souff.setText("{0:.1f} ℃".format(data.wind_data["instaneous"].Tflow))
        self.temperatureAirLabel_hall.setText("{0:.1f} ℃".format(data.wind_data["instaneous"].Tatm))
        self.humiditeRelLabel.setText("{0:.0f} %".format(data.wind_data["instaneous"].RHatm*100))
        # self.dPFlowLabel.setText("{0:.1f} Pa".format(data.wind_data["instaneous"].dPflow))
        self.veLabel_uFlow.setText("{0:.1f} m/s".format(data.wind_data["instaneous"].Uflow))
        # TODO:
        self.veLabel_uFlowMean.setText("{0:.1f} m/s".format(data.wind_data["instaneous"].U_flow))

        # tunnel data
        # self.moteurInfoLabel_poweredInfo.setText("{}".format(data.tunnel_data["instaneous"].powered))
        # self.moteurInfoLabel_controlledInfo.setText("{}".format(data.tunnel_data["instaneous"].controlled))
        # self.moteurInfoLabel_statusInfo.setText("{}".format(data.tunnel_data["instaneous"].status))
        # self.moteurInfoLabel_messageInfo.setText("{}".format(data.tunnel_data["instaneous"].message))

        if data.tunnel_data["instaneous"].powered == "On":
            self.powerLightButton.setStyleSheet("QPushButton {background-color: green; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        else:
            self.powerLightButton.setStyleSheet("QPushButton {background-color: red; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")

        if data.tunnel_data["instaneous"].status == True:
            self.statusLightButton.setStyleSheet("QPushButton {background-color: green; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        elif data.tunnel_data["instaneous"].status == False:
            self.statusLightButton.setStyleSheet("QPushButton {background-color: grey; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        else:
            self.statusLightButton.setStyleSheet("QPushButton {background-color: orange; \
                                                    border-radius:8px; \
                                                    max-width:16px; \
                                                    max-height:16px;}")

        if data.tunnel_data["instaneous"].controlled == "Auto":
            self.controlLightButton.setStyleSheet("QPushButton {background-color: green; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        else:
            self.controlLightButton.setStyleSheet("QPushButton {background-color: red; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")


        self.VMLabel_consigne.setText("{0:.0f} RPM".format(data.tunnel_data["instaneous"].RPM))
        self.VMLabel_mesure.setText("{0:.1f} RPM".format(data.tunnel_data["instaneous"].rpm))


    def showTimeReel(self):
        datetime = PyQCore.QDateTime.currentDateTime()
        time_str = datetime.toString("yyyy-MM-dd\nhh:mm:ss")
        self.timeReelButton.setText("Temps Actuel:\n"+time_str)
    def showTimeLastUpdate(self):
        datetime = PyQCore.QDateTime.currentDateTime()
        time_str = datetime.toString("yyyy-MM-dd\nhh:mm:ss")
        self.timeLastShowButton.setText("Temps Mesure:\n"+time_str)




    def addVitesseMoteurWidget(self):
        vmWidget = PyQW.QWidget(self)
        vmLayout = PyQW.QVBoxLayout(vmWidget)
        titleButton = PyQW.QPushButton("Vitesse Moteur", vmWidget)
        titleButton.setStyleSheet("QPushButton {"+self.preferences.otherSettings.qss_centralWinTitleButton+"}")
                                    # border-style have many types 'outset' or 'solid'

        vmLayoutH = PyQW.QHBoxLayout(vmWidget)
        consigneButton = PyQW.QPushButton("CONSIGNE", vmWidget)
        mesureButton = PyQW.QPushButton("MESURE", vmWidget)
        consigneButton.setStyleSheet("QPushButton{"+self.preferences.otherSettings.qss_centralWin_subButton+"}")
        mesureButton.setStyleSheet("QPushButton{" + self.preferences.otherSettings.qss_centralWin_subButton + "}")

        vmLayoutH.addWidget(consigneButton)
        vmLayoutH.addWidget(mesureButton)

        vmLayoutH2 = PyQW.QHBoxLayout(vmWidget)
        self.VMLabel_consigne = PyQW.QLabel(vmWidget)
        self.VMLabel_mesure = PyQW.QLabel(vmWidget)
        vmLayoutH2.addWidget(self.VMLabel_consigne)
        vmLayoutH2.addWidget(self.VMLabel_mesure)
        # make them align center.
        self.VMLabel_consigne.setAlignment(PyQCore.Qt.AlignCenter)
        self.VMLabel_mesure.setAlignment(PyQCore.Qt.AlignCenter)
        self.VMLabel_consigne.setText("{} RPM".format("---"))
        self.VMLabel_mesure.setText("{} RPM".format("---"))

        vmLayoutH3 = PyQW.QHBoxLayout(vmWidget)
        self.statusButton = PyQW.QPushButton("Status", vmWidget)
        self.powerButton = PyQW.QPushButton("Power", vmWidget)
        self.controlButton = PyQW.QPushButton("Control", vmWidget)
        self.statusLightButton = PyQW.QPushButton(vmWidget)
        self.powerLightButton = PyQW.QPushButton(vmWidget)
        self.controlLightButton = PyQW.QPushButton(vmWidget)

        self.statusLightButton.setStyleSheet("QPushButton {background-color: gray; \
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        self.powerLightButton.setStyleSheet("QPushButton {background-color: gray; \
                                        border-style: solid;\
                                        border-width:1px;\
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        self.controlLightButton.setStyleSheet("QPushButton {background-color: gray; \
                                        border-style: solid;\
                                        border-width:1px;\
                                        border-radius:8px; \
                                        max-width:16px; \
                                        max-height:16px;}")
        self.statusButton.setStyleSheet("QPushButton {"+self.preferences.otherSettings.qss_moteurInfo+"}")
        self.powerButton.setStyleSheet("QPushButton {"+self.preferences.otherSettings.qss_moteurInfo+"}")
        self.controlButton.setStyleSheet("QPushButton {"+self.preferences.otherSettings.qss_moteurInfo+"}")
        vmLayoutH3.addWidget(self.statusButton)
        vmLayoutH3.addWidget(self.statusLightButton)
        vmLayoutH3.addWidget(self.powerButton)
        vmLayoutH3.addWidget(self.powerLightButton)
        vmLayoutH3.addWidget(self.controlButton)
        vmLayoutH3.addWidget(self.controlLightButton)

        # vmLayoutH3 = PyQW.QHBoxLayout(vmWidget)
        # self.VMLabel_statusLabel = PyQW.QLabel(vmWidget)
        # self.VMLabel_powerLabel = PyQW.QLabel(vmWidget)
        # self.VMLabel_controlLabel = PyQW.QLabel(vmWidget)
        # vmLayoutH3.addWidget(self.VMLabel_statusLabel)
        # vmLayoutH3.addWidget(self.VMLabel_powerLabel)
        # vmLayoutH3.addWidget(self.VMLabel_controlLabel)
        # # make them align center.
        # self.VMLabel_statusLabel.setAlignment(PyQCore.Qt.AlignCenter)
        # self.VMLabel_powerLabel.setAlignment(PyQCore.Qt.AlignCenter)
        # self.VMLabel_controlLabel.setAlignment(PyQCore.Qt.AlignCenter)
        # self.VMLabel_statusLabel.setText("Status")
        # self.VMLabel_powerLabel.setText("Power")
        # self.VMLabel_controlLabel.setText("Control")

        vmLayout.addWidget(titleButton)
        vmLayout.addLayout(vmLayoutH)
        vmLayout.addLayout(vmLayoutH2)
        vmLayout.addLayout(vmLayoutH3)
        vmWidget.setLayout(vmLayout)

        vmWidget.setObjectName("vmWidget")
        # add border for vmWidget and two Qlabels
        vmWidget.setStyleSheet("QWidget#vmWidget {background-color: white;}"
                                "QWidget#vmWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {"+ self.preferences.otherSettings.qss_centralWin_afficheDonne+"}")
        vmWidget.setStatusTip("Vitesse Moteur")
        return vmWidget

    def addTemperatureAirWidget(self):
        taWidget = PyQW.QWidget(self)
        vmLayout = PyQW.QVBoxLayout(taWidget)
        titleButton = PyQW.QPushButton("Température de l'air", taWidget)
        titleButton.setStyleSheet("QPushButton {" + self.preferences.otherSettings.qss_centralWinTitleButton + "}")

        vmLayoutH = PyQW.QHBoxLayout(taWidget)
        soufflerieButton = PyQW.QPushButton("SOUFFLERIE", taWidget)
        hallButton = PyQW.QPushButton("HALL", taWidget)
        soufflerieButton.setStyleSheet("QPushButton{"+self.preferences.otherSettings.qss_centralWin_subButton+"}")
        hallButton.setStyleSheet("QPushButton{" + self.preferences.otherSettings.qss_centralWin_subButton + "}")
        vmLayoutH.addWidget(hallButton)
        vmLayoutH.addWidget(soufflerieButton)

        vmLayoutH2 = PyQW.QHBoxLayout(taWidget)
        self.temperatureAirLabel_souff = PyQW.QLabel(taWidget)
        self.temperatureAirLabel_hall = PyQW.QLabel(taWidget)
        vmLayoutH2.addWidget(self.temperatureAirLabel_hall)
        vmLayoutH2.addWidget(self.temperatureAirLabel_souff)

        self.temperatureAirLabel_souff.setText("{} ℃".format("---"))
        self.temperatureAirLabel_hall.setText("{} ℃".format("---"))

        self.temperatureAirLabel_souff.setAlignment(PyQCore.Qt.AlignCenter)
        self.temperatureAirLabel_hall.setAlignment(PyQCore.Qt.AlignCenter)


        vmLayout.addWidget(titleButton)
        vmLayout.addLayout(vmLayoutH)
        vmLayout.addLayout(vmLayoutH2)
        taWidget.setLayout(vmLayout)

        taWidget.setObjectName("taWidget")
        # add border for taWidget and two Qlabels
        taWidget.setStyleSheet("QWidget#taWidget {background-color: white;}"
                                "QWidget#taWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {"+ self.preferences.otherSettings.qss_centralWin_afficheDonne+"}")
        taWidget.setStatusTip("Température de l'air")
        titleButton.clicked.connect(lambda: self.signalEnsemble.openTemperatureWin.emit())
        soufflerieButton.clicked.connect(lambda: self.signalEnsemble.openTemperatureWin.emit())


        return taWidget

    def addPressionAtmoWidget(self):
        paWidget = PyQW.QWidget(self)
        vmLayout = PyQW.QVBoxLayout(paWidget)
        titleButton = PyQW.QPushButton("Pression Atmosphérique", paWidget)
        titleButton.setStyleSheet("QPushButton {" + self.preferences.otherSettings.qss_centralWinTitleButton + "}")
        vmLayoutH = PyQW.QHBoxLayout(paWidget)
        soufflerieButton = PyQW.QPushButton("SOUFFLERIE", paWidget)
        hallButton = PyQW.QPushButton("HALL", paWidget)
        soufflerieButton.setStyleSheet("QPushButton{"+self.preferences.otherSettings.qss_centralWin_subButton+"}")
        hallButton.setStyleSheet("QPushButton{" + self.preferences.otherSettings.qss_centralWin_subButton + "}")
        vmLayoutH.addWidget(hallButton)
        vmLayoutH.addWidget(soufflerieButton)


        vmLayoutH2 = PyQW.QHBoxLayout(paWidget)
        self.pressAtomLabel_souff = PyQW.QLabel(paWidget)
        self.pressAtomLabel_hall = PyQW.QLabel(paWidget)
        vmLayoutH2.addWidget(self.pressAtomLabel_hall)
        vmLayoutH2.addWidget(self.pressAtomLabel_souff)

        self.pressAtomLabel_souff.setText("{} Pa".format("---"))
        self.pressAtomLabel_hall.setText("{} Pa".format("---"))

        self.pressAtomLabel_souff.setAlignment(PyQCore.Qt.AlignCenter)
        self.pressAtomLabel_hall.setAlignment(PyQCore.Qt.AlignCenter)


        vmLayout.addWidget(titleButton)
        vmLayout.addLayout(vmLayoutH)
        vmLayout.addLayout(vmLayoutH2)
        paWidget.setLayout(vmLayout)

        paWidget.setObjectName("paWidget")
        # add border for paWidget and two Qlabels
        paWidget.setStyleSheet("QWidget#paWidget {background-color: white;}"
                                "QWidget#paWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {"+ self.preferences.otherSettings.qss_centralWin_afficheDonne+"}")
        paWidget.setStatusTip("Pression Atmosphérique")
        titleButton.clicked.connect(lambda: self.signalEnsemble.openPressWin.emit())
        soufflerieButton.clicked.connect(lambda: self.signalEnsemble.openPressWin.emit())
        return paWidget



    def addMoteurInfoWidget(self):
        miWidget = PyQW.QWidget(self)
        vmLayout = PyQW.QVBoxLayout(miWidget)
        titleButton = PyQW.QPushButton("Moteur Info", miWidget)
        titleButton.setStyleSheet("QPushButton {" + self.preferences.otherSettings.qss_centralWinTitleButton + "}")
        vmLayoutH = PyQW.QHBoxLayout(miWidget)
        moteurInfoLabel_powered = PyQW.QLabel(miWidget)
        self.moteurInfoLabel_poweredInfo = PyQW.QLabel(miWidget)

        vmLayoutH.addWidget(moteurInfoLabel_powered)
        vmLayoutH.addWidget(self.moteurInfoLabel_poweredInfo)
        moteurInfoLabel_powered.setText("Powered:")
        self.moteurInfoLabel_poweredInfo.setText("---")

        moteurInfoLabel_powered.setAlignment(PyQCore.Qt.AlignRight)
        self.moteurInfoLabel_poweredInfo.setAlignment(PyQCore.Qt.AlignLeft)

        ###
        vmLayoutH2 = PyQW.QHBoxLayout(miWidget)
        moteurInfoLabel_status = PyQW.QLabel(miWidget)
        self.moteurInfoLabel_statusInfo = PyQW.QLabel(miWidget)

        vmLayoutH2.addWidget(moteurInfoLabel_status)
        vmLayoutH2.addWidget(self.moteurInfoLabel_statusInfo)
        moteurInfoLabel_status.setText("Status:")
        self.moteurInfoLabel_statusInfo.setText("---")
        moteurInfoLabel_status.setAlignment(PyQCore.Qt.AlignRight)
        self.moteurInfoLabel_statusInfo.setAlignment(PyQCore.Qt.AlignLeft)

        vmLayoutH3 = PyQW.QHBoxLayout(miWidget)
        moteurInfoLabel_controlled = PyQW.QLabel(miWidget)
        self.moteurInfoLabel_controlledInfo = PyQW.QLabel(miWidget)
        vmLayoutH3.addWidget(moteurInfoLabel_controlled)
        vmLayoutH3.addWidget(self.moteurInfoLabel_controlledInfo)
        moteurInfoLabel_controlled.setText("Controlled:")
        self.moteurInfoLabel_controlledInfo.setText("---")
        moteurInfoLabel_controlled.setAlignment(PyQCore.Qt.AlignRight)
        self.moteurInfoLabel_controlledInfo.setAlignment(PyQCore.Qt.AlignLeft)

        vmLayoutH4 = PyQW.QHBoxLayout(miWidget)
        moteurInfoLabel_message = PyQW.QLabel(miWidget)
        self.moteurInfoLabel_messageInfo = PyQW.QLabel(miWidget)
        vmLayoutH4.addWidget(moteurInfoLabel_message)
        vmLayoutH4.addWidget(self.moteurInfoLabel_messageInfo)
        moteurInfoLabel_message.setText("Message:")
        self.moteurInfoLabel_messageInfo.setText("---")
        moteurInfoLabel_message.setAlignment(PyQCore.Qt.AlignRight)
        self.moteurInfoLabel_messageInfo.setAlignment(PyQCore.Qt.AlignLeft)


        vmLayout.addWidget(titleButton)
        vmLayout.addLayout(vmLayoutH)
        vmLayout.addLayout(vmLayoutH2)
        vmLayout.addLayout(vmLayoutH3)
        vmLayout.addLayout(vmLayoutH4)
        miWidget.setLayout(vmLayout)

        miWidget.setObjectName("miWidget")
        # add border for miWidget and two Qlabels
        miWidget.setStyleSheet("QWidget#miWidget {background-color: white;}"
                                "QWidget#miWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {color: black;font-family:Times New Roman}")

        miWidget.setStatusTip("Vitesse de référence")



        return miWidget

    def addHumiditeRelativeWidget(self):
        hrWidget = PyQW.QWidget(self)
        vmLayout = PyQW.QVBoxLayout(hrWidget)
        titleButton = PyQW.QPushButton("Humidité Relative", hrWidget)
        titleButton.setStyleSheet("QPushButton {" + self.preferences.otherSettings.qss_centralWinTitleButton + "}")


        self.humiditeRelLabel = PyQW.QLabel(hrWidget)

        self.humiditeRelLabel.setText("{} %".format("---"))

        # make them align center.
        self.humiditeRelLabel.setAlignment(PyQCore.Qt.AlignCenter)


        vmLayout.addWidget(titleButton)
        vmLayout.addWidget(self.humiditeRelLabel)

        hrWidget.setLayout(vmLayout)

        hrWidget.setObjectName("hrWidget")
        # add border for hrWidget and two Qlabels
        hrWidget.setStyleSheet("QWidget#hrWidget {background-color: white;}"
                                "QWidget#hrWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {"+self.preferences.otherSettings.qss_centralWin_afficheDonne+"}")
        hrWidget.setStatusTip("Humidité Relative")
        titleButton.clicked.connect(lambda: self.signalEnsemble.openHumiditeRelWin.emit())


        return hrWidget


    def adddPFlowWidget(self):
        dPWidget = PyQW.QWidget(self)
        vmLayout = PyQW.QVBoxLayout(dPWidget)
        titleButton = PyQW.QPushButton("dPflow", dPWidget)
        titleButton.setStyleSheet("QPushButton {background-color: #48D1CC;}"
                                  "QPushButton {color: white; font-size:12px; font-weight: bold; font-family:monaco;}"
                                  "QPushButton {border-style: solid; padding: 2px; border-width: 2px;border-color:#48D1CC;}")
                                    # border-style have many types 'outset' or 'solid'


        self.dPFlowLabel = PyQW.QLabel(dPWidget)

        self.dPFlowLabel.setText("{} Pa".format("---"))

        # make them align center.
        self.dPFlowLabel.setAlignment(PyQCore.Qt.AlignCenter)


        vmLayout.addWidget(titleButton)
        vmLayout.addWidget(self.dPFlowLabel)

        dPWidget.setLayout(vmLayout)

        dPWidget.setObjectName("dPWidget")
        # add border for dPWidget and two Qlabels
        dPWidget.setStyleSheet("QWidget#dPWidget {background-color: white;}"
                                "QWidget#dPWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {border-style:solid; border-width: 1px; border-color:#48D1CC;font-family:Times New Roman}")
        dPWidget.setStatusTip("Humidité Relative")

        titleButton.clicked.connect(lambda: self.signalEnsemble.openPressionDiffWin.emit())



        return dPWidget

    def addVitesseEcoulementWidget(self):
        veWidget = PyQW.QWidget(self)
        veLayout = PyQW.QVBoxLayout(veWidget)
        titleButton = PyQW.QPushButton("Vitesse d'écoulement de référence", veWidget)
        titleButton.setStyleSheet("QPushButton {" + self.preferences.otherSettings.qss_centralWinTitleButton + "}")

        veLayoutH = PyQW.QHBoxLayout(veWidget)
        uFlowButton = PyQW.QPushButton("uFlow", veWidget)
        uFlowMeanButton = PyQW.QPushButton("uFlowMean", veWidget)
        uFlowButton.setStyleSheet("QPushButton{"+self.preferences.otherSettings.qss_centralWin_subButton+"}")
        uFlowMeanButton.setStyleSheet("QPushButton{" + self.preferences.otherSettings.qss_centralWin_subButton + "}")
        veLayoutH.addWidget(uFlowButton)
        veLayoutH.addWidget(uFlowMeanButton)

        veLayoutH2 = PyQW.QHBoxLayout(veWidget)
        self.veLabel_uFlow = PyQW.QLabel(veWidget)
        self.veLabel_uFlowMean = PyQW.QLabel(veWidget)
        veLayoutH2.addWidget(self.veLabel_uFlow)
        veLayoutH2.addWidget(self.veLabel_uFlowMean)
        # make them align center.
        self.veLabel_uFlow.setAlignment(PyQCore.Qt.AlignCenter)
        self.veLabel_uFlowMean.setAlignment(PyQCore.Qt.AlignCenter)
        self.veLabel_uFlow.setText("{} m/s".format("---"))
        self.veLabel_uFlowMean.setText("{} m/s".format("---"))

        veLayout.addWidget(titleButton)
        veLayout.addLayout(veLayoutH)
        veLayout.addLayout(veLayoutH2)
        veWidget.setLayout(veLayout)

        veWidget.setObjectName("veWidget")
        # add border for veWidget and two Qlabels
        veWidget.setStyleSheet("QWidget#veWidget {background-color: white;}"
                                "QWidget#veWidget {border-style: solid; border-width:2px; border-color:black;}"
                               "QLabel {"+ self.preferences.otherSettings.qss_centralWin_afficheDonne+"}")
        veWidget.setStatusTip("Vitesse d'écoulement")
        titleButton.clicked.connect(lambda: self.signalEnsemble.openUflowWin.emit())
        uFlowButton.clicked.connect(lambda: self.signalEnsemble.openUflowWin.emit())
        uFlowMeanButton.clicked.connect(lambda: self.signalEnsemble.openUflowMeanWin.emit())
        return veWidget


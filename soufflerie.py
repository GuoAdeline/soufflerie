# Copyright 2019: CAI Pengfei, GUO Mengxue. All Rights Reserved.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public version 2 or later
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ======================================================================
import sys
import PyQt5.QtWidgets as PyQW
import PyQt5.QtGui as PyQGui
import PyQt5.QtCore as PyQCore
import data_skeleton
import subwins
import affiche_win

class SignalEnsemble(PyQCore.QObject):
    openPressWin = PyQCore.pyqtSignal()
    openTemperatureWin = PyQCore.pyqtSignal()
    openPressionDiffWin = PyQCore.pyqtSignal()
    openHumiditeRelWin = PyQCore.pyqtSignal()
    openUflowWin = PyQCore.pyqtSignal()
    openUflowMeanWin = PyQCore.pyqtSignal()





class Example(PyQW.QMainWindow):

    def __init__(self):
        super().__init__()
        self.timer1 = PyQCore.QTimer(self)
        self.timer1.start(1000)    # every 1000 millisecond(1s), update time actuel
        self.timer2 = PyQCore.QTimer(self)
        self.timer2.start(5000)   # default: every 5s update value
        # self.timer2.start(1000)
        self.signalEnsemble = SignalEnsemble()

        self.dataH = data_skeleton.HistData()
        self.dataH.history()    # used to save history

        self.aboutWin = subwins.AboutWin()
        self.preferenceWin = subwins.PreferenceWin(self.timer2)

        # self.vitessEcoulementWin = subwins.VitesseEcoulementWin()
        self.pressionWin = affiche_win.PressionWin(self.timer2, self.dataH, self.preferenceWin)
        self.temperatureWin = affiche_win.TemperatureWin(self.timer2, self.dataH, self.preferenceWin)
        self.pressionDiffWin = affiche_win.PressionDiffWin(self.timer2, self.dataH, self.preferenceWin)
        self.HumiditeRelWin = affiche_win.HumiditeRelWin(self.timer2, self.dataH, self.preferenceWin)
        self.uflowWin = affiche_win.VitesseEcoulementWin(self.timer2, self.dataH, self.preferenceWin)
        self.uflowMeanWin = affiche_win.VitesseEcoulementMeanWin(self.timer2, self.dataH, self.preferenceWin)



        self.main_content = subwins.centralWin(self.timer1, self.timer2, self.dataH, self.signalEnsemble, self.preferenceWin)

        self.signalEnsemble.openPressWin.connect(lambda : self.showPressionWin())
        self.signalEnsemble.openTemperatureWin.connect(lambda : self.showTemperatureWin())
        self.signalEnsemble.openPressionDiffWin.connect(lambda : self.showPressionDiffWin())
        self.signalEnsemble.openHumiditeRelWin.connect(lambda : self.showHumiditeRelWin())
        self.signalEnsemble.openUflowWin.connect(lambda : self.showUflowWin())
        self.signalEnsemble.openUflowMeanWin.connect(lambda : self.showUflowMeanWin())


        print(self.preferenceWin.historique_affiche)
        print(self.preferenceWin.ipAddr)

        self.initUI()

    def initUI(self):

        self.statusBar().showMessage('Ready')
        self.menubarAndToolbar()
        self.centralWidget()

        self.setCentralWidget(self.main_content)


        self.setGeometry(150, 50, subwins.holdScale(800), 800)
        self.setWindowTitle('Soufflerie')
        self.setWindowIcon(PyQGui.QIcon('./figs/fan.png'))

        self.show()

    def showAboutWin(self):
        self.aboutWin.show()

    def showPreferenceWin(self):
        self.preferenceWin.show()

    def showUflowWin(self):
        self.uflowWin.show()
    def showUflowMeanWin(self):
        self.uflowMeanWin.show()

    def showPressionWin(self):
        self.pressionWin.show()

    def showPressionDiffWin(self):
        self.pressionDiffWin.show()

    def showTemperatureWin(self):
        self.temperatureWin.show()

    def showHumiditeRelWin(self):
        self.HumiditeRelWin.show()
    def reloadData(self):
        if(not self.timer2.isActive()):
            self.warningMsg("You haven't set scan time")
            pass # you haven't set scan time
        else:
            self.main_content.showTimeLastUpdate()
            self.main_content.updateAllParam()

    def menubarAndToolbar(self):
        # menubars
        menubar = self.menuBar()
        fileMenu = menubar.addMenu(PyQGui.QIcon("./figs/menu.png"), "MENU")


        preferenceAct = PyQW.QAction('Préférences', self)
        preferenceAct.triggered.connect(self.showPreferenceWin)
        preferenceAct.setStatusTip("Préférences des données")

        miseAJourAct = PyQW.QAction(PyQGui.QIcon('./figs/reload.png'),'Mise à jour', self)
        miseAJourAct.triggered.connect(self.reloadData)
        miseAJourAct.setStatusTip("Rafraîchir immédiatement")

        fileMenu.addAction(preferenceAct)
        fileMenu.addAction(miseAJourAct)

        # submenu affichage d'historique
        affichageMenu = PyQW.QMenu("Affichage d'historique", self)
        # vitesseEcoulementAct = PyQW.QAction("Vitesse d'écoulement", self)
        # vitesseEcoulementAct.triggered.connect(self.showVitesseEcoulementWin)

        pressionAct = PyQW.QAction("Pression", self)
        pressionAct.triggered.connect(self.showPressionWin)

        temperatureAct = PyQW.QAction("Température", self)
        temperatureAct.triggered.connect(self.showTemperatureWin)

        HumiditeRelAct = PyQW.QAction("Humidité Relative",self)
        HumiditeRelAct.triggered.connect(self.showHumiditeRelWin)

        vitesseEcoulementAct = PyQW.QAction("Vitesse d'écoulement", self)
        vitesseEcoulementAct.triggered.connect(lambda: self.signalEnsemble.openUflowMeanWin.emit())
        # affichageMenu.addAction(vitesseEcoulementAct)
        affichageMenu.addAction(pressionAct)
        affichageMenu.addAction(temperatureAct)
        affichageMenu.addAction(HumiditeRelAct)
        affichageMenu.addAction(vitesseEcoulementAct)



        # add submenu to fileMenu
        fileMenu.addMenu(affichageMenu)

        exitAct = PyQW.QAction(PyQGui.QIcon('./figs/exit.png'), 'Quitter', self)
        exitAct.triggered.connect(PyQW.qApp.quit)
        exitAct.setStatusTip("Quitter l'application")
        fileMenu.addAction(exitAct)

        # toolbar: exit, reload, and aide
        self.spacer = PyQW.QWidget()
        self.spacer.setSizePolicy(PyQW.QSizePolicy.Expanding, PyQW.QSizePolicy.Expanding)

        aboutAct = PyQW.QAction(PyQGui.QIcon('./figs/question.png'), 'Aide', self)
        aboutAct.triggered.connect(self.showAboutWin)
        aboutAct.setStatusTip("À propos")
        fileMenu.addAction(aboutAct)



        self.toolbar = self.addToolBar('Aide')
        self.toolbar.addAction(exitAct)
        self.toolbar.addAction(miseAJourAct)
        self.toolbar.addWidget(self.spacer)
        self.toolbar.addAction(aboutAct)

    def closeEvent(self, event):
        reply = PyQW.QMessageBox.question(self,"Soufflerie", "Êtes-vous sûr de vouloir quitter?",
                                     PyQW.QMessageBox.Yes|PyQW.QMessageBox.No, PyQW.QMessageBox.No)
        if reply==PyQW.QMessageBox.Yes:
            event.accept()
            sys.exit(0) # exit all
        else:
            event.ignore()

    def warningMsg(self, msg):
        reply = PyQW.QMessageBox.information(self,  # infomation subwin
                                        "Warning",
                                        msg,
                                        PyQW.QMessageBox.Ok)



if __name__ == '__main__':
    app = PyQW.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())

